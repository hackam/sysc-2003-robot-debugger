/*
 * 
 * GUI.java , a GUI interface for SYSC 2003 Robot Debugger
 * @author Amente Bekele & David Yao
 * @verison April-8-2013 
 * 
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GUI {

	JTextArea txtLog;
	JLabel lblStatusLabel;

	JCheckBox chckbxLed;
	JCheckBox chckbxLed_1;
	JCheckBox chckbxLed_2;
	JCheckBox chckbxLed_3;
	JCheckBox chckbxBuzzer;
	JButton btnConnect;
	JTextField textCmd;
	JCheckBox chckbxRelay;
	JComboBox<Integer> combo7seg;
	JLabel lblDutyValue;
	JButton btnUpdateLcd;
	JButton btnClearLcd;
	JButton btnCw;
	JButton btnCcw;
	JButton btnPullKeypad;
	JLabel lblKeypad;
	JLabel lblRps;
	AboutDialog aboutDialog;

	JComboBox<Object> cBoxPorts;

	Communicator communicator = null;

	private JFrame frmSyscRobot;
	private JTextField textField_1;
	private JTextField textField;

	private final JMenuBar menuBar = new JMenuBar();
	private final JMenu mnFile = new JMenu("File");
	private final JMenu mnViews = new JMenu("About");

	private final JMenuItem closeWindow = new JMenuItem("Close");
	private final JMenuItem mntmClear = new JMenuItem("Clear");
	private final JPopupMenu popupMenu = new JPopupMenu();
	private final JMenuItem aboutMenu = new JMenuItem("About");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmSyscRobot.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
		aboutDialog = new AboutDialog();
		communicator = new Communicator(this);
		communicator.searchForPorts();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSyscRobot = new JFrame();
		frmSyscRobot.setResizable(false);
		frmSyscRobot.setTitle("SYSC 2003 Robot Debugger");
		frmSyscRobot.setBounds(100, 100, 600, 720);
		frmSyscRobot.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSyscRobot.getContentPane().setLayout(null);

		ButtonListener listener = new ButtonListener();

		JPanel panel_3 = new JPanel();
		panel_3.setBounds(0, 632, 594, 24);
		frmSyscRobot.getContentPane().add(panel_3);

		lblStatusLabel = new JLabel("Disconnected");
		panel_3.add(lblStatusLabel);

		JPanel panel_4 = new JPanel();
		panel_4.setBounds(0, 43, 594, 589);
		panel_4.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Controls",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frmSyscRobot.getContentPane().add(panel_4);
		panel_4.setLayout(null);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(10, 25, 574, 46);
		panel_2.setBorder(new TitledBorder(null, "Port K",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));

		Component horizontalGlue_5 = Box.createHorizontalGlue();
		panel_2.add(horizontalGlue_5);

		chckbxLed = new JCheckBox("LED 0");
		panel_2.add(chckbxLed);
		chckbxLed.addActionListener(listener);

		Component horizontalGlue = Box.createHorizontalGlue();
		panel_2.add(horizontalGlue);

		chckbxLed_1 = new JCheckBox("LED 1");
		panel_2.add(chckbxLed_1);
		chckbxLed_1.addActionListener(listener);

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		panel_2.add(horizontalGlue_1);

		chckbxLed_2 = new JCheckBox("LED 2");
		panel_2.add(chckbxLed_2);
		chckbxLed_2.addActionListener(listener);

		Component horizontalGlue_2 = Box.createHorizontalGlue();
		panel_2.add(horizontalGlue_2);

		chckbxLed_3 = new JCheckBox("LED 3");
		panel_2.add(chckbxLed_3);
		chckbxLed_3.addActionListener(listener);

		Component horizontalGlue_3 = Box.createHorizontalGlue();
		panel_2.add(horizontalGlue_3);

		chckbxRelay = new JCheckBox("Relay");
		panel_2.add(chckbxRelay);
		chckbxRelay.addActionListener(listener);

		Component horizontalGlue_4 = Box.createHorizontalGlue();
		panel_2.add(horizontalGlue_4);

		chckbxBuzzer = new JCheckBox("Buzzer");
		panel_2.add(chckbxBuzzer);
		chckbxBuzzer.addActionListener(listener);

		Component horizontalGlue_6 = Box.createHorizontalGlue();
		panel_2.add(horizontalGlue_6);

		JPanel panel_5 = new JPanel();
		panel_5.setBounds(10, 82, 343, 94);
		panel_5.setBorder(new TitledBorder(null, "LCD", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel_4.add(panel_5);
		panel_5.setAlignmentY(Component.TOP_ALIGNMENT);
		panel_5.setLayout(null);

		JPanel panel_7 = new JPanel();
		panel_7.setBounds(10, 22, 323, 61);
		panel_5.add(panel_7);
		panel_7.setLayout(null);

		textField_1 = new JTextField();
		textField_1.setBounds(0, 0, 214, 23);
		panel_7.add(textField_1);
		textField_1.setColumns(16);

		btnUpdateLcd = new JButton("Update");
		btnUpdateLcd.addActionListener(listener);
		btnUpdateLcd.setBounds(224, 0, 89, 23);
		panel_7.add(btnUpdateLcd);

		

		btnClearLcd = new JButton("Clear");
		btnClearLcd.addActionListener(listener);
		btnClearLcd.setBounds(224, 34, 89, 23);
		panel_7.add(btnClearLcd);

		JPanel panel_6 = new JPanel();
		panel_6.setBounds(10, 287, 574, 54);
		panel_6.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Commands",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.add(panel_6);
		panel_6.setLayout(null);

		textCmd = new JTextField();
		textCmd.setBorder(new LineBorder(new Color(0, 0, 0)));
		textCmd.setBounds(10, 17, 455, 23);
		panel_6.add(textCmd);

		JButton btnSend = new JButton("Send");
		btnSend.setBounds(475, 17, 89, 23);
		panel_6.add(btnSend);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 352, 574, 238);
		panel_1.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Log", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel_4.add(panel_1);
		panel_1.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 16, 558, 215);
		scrollPane.setBorder(null);
		panel_1.add(scrollPane);

		txtLog = new JTextArea();
		txtLog.setBorder(new LineBorder(new Color(0, 0, 0)));
		txtLog.setEditable(false);
		scrollPane.setViewportView(txtLog);

		JPanel panel_8 = new JPanel();
		panel_8.setBounds(363, 82, 221, 64);
		panel_8.setBorder(new TitledBorder(null, "7 Segment Display",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.add(panel_8);
		panel_8.setLayout(null);

		combo7seg = new JComboBox<Integer>();
		combo7seg.setBounds(83, 33, 70, 20);

		for (int i = 0; i < 10; i++) {
			combo7seg.addItem(i);
		}
		combo7seg.addActionListener(listener);
		panel_8.add(combo7seg);

		JPanel panel_9 = new JPanel();
		panel_9.setBounds(363, 149, 221, 72);
		panel_9.setBorder(new TitledBorder(null, "Keypad",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.add(panel_9);
		panel_9.setLayout(null);

		JLabel lblResult = new JLabel("Result: ");
		lblResult.setBounds(78, 11, 46, 14);
		panel_9.add(lblResult);

		lblKeypad = new JLabel("N/A");
		lblKeypad.setBounds(129, 11, 38, 14);
		panel_9.add(lblKeypad);

		btnPullKeypad = new JButton("Get");
		btnPullKeypad.setBounds(78, 36, 89, 23);
		panel_9.add(btnPullKeypad);
		btnPullKeypad.addActionListener(listener);

		JPanel panel_10 = new JPanel();
		panel_10.setBounds(10, 182, 343, 105);
		panel_10.setBorder(new TitledBorder(null, "DC Motor",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.add(panel_10);
		panel_10.setLayout(null);

		JPanel panel_11 = new JPanel();
		panel_11.setBounds(10, 17, 323, 23);
		panel_10.add(panel_11);
		panel_11.setLayout(new BoxLayout(panel_11, BoxLayout.X_AXIS));

		Component horizontalGlue_8 = Box.createHorizontalGlue();
		panel_11.add(horizontalGlue_8);

		JLabel lblDuty = new JLabel("Duty");
		panel_11.add(lblDuty);

		Component horizontalGlue_7 = Box.createHorizontalGlue();
		panel_11.add(horizontalGlue_7);

		lblRps = new JLabel("RPS:0");
		panel_11.add(lblRps);

		Component horizontalGlue_9 = Box.createHorizontalGlue();
		panel_11.add(horizontalGlue_9);

		JSlider slider = new JSlider();
		slider.setValue(0);
		slider.addChangeListener(listener);
		slider.setBounds(46, 49, 289, 45);
		panel_10.add(slider);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);

		lblDutyValue = new JLabel(slider.getValue() + "%");
		lblDutyValue.setBounds(20, 59, 36, 14);
		panel_10.add(lblDutyValue);

		JPanel panel_12 = new JPanel();
		panel_12.setBounds(363, 232, 221, 54);
		panel_12.setBorder(new TitledBorder(null, "Stepper Motor",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.add(panel_12);
		panel_12.setLayout(new GridLayout(0, 2, 0, 0));

		btnCw = new JButton("CW");
		btnCw.addActionListener(listener);
		panel_12.add(btnCw);

		btnCcw = new JButton("CCW");
		btnCcw.addActionListener(listener);
		panel_12.add(btnCcw);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 594, 43);
		panel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Connection",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frmSyscRobot.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel label = new JLabel("Port: ");
		label.setBounds(124, 15, 40, 14);
		panel.add(label);

		cBoxPorts = new JComboBox<Object>();
		cBoxPorts.setMinimumSize(new Dimension(50, 20));
		cBoxPorts.setBounds(161, 12, 73, 20);
		panel.add(cBoxPorts);

		JLabel label_1 = new JLabel("Baud: ");
		label_1.setBounds(263, 15, 40, 14);
		panel.add(label_1);

		textField = new JTextField();
		textField.setText("9600");
		textField.setColumns(10);
		textField.setBounds(322, 12, 73, 20);
		panel.add(textField);

		btnConnect = new JButton("Connect");
		btnConnect.setBounds(405, 11, 100, 23);
		panel.add(btnConnect);

		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (communicator.getConnected() == false) {
					communicator.connect();
					if (communicator.initIOStream() == true) {
						communicator.initListener();
						btnConnect.setText("Disconnect");
						lblStatusLabel.setText("Connected");
					}
				} else {
					communicator.disconnect();
					btnConnect.setText("Connect");
					lblStatusLabel.setText("Not connected");
				}

			}
		});

		closeWindow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				communicator.disconnect();
				System.exit(0);
			}
		});
		
		aboutMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				aboutDialog.setVisible(true);
			}
		});

		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				communicator.writeData(textCmd.getText());
			}
		});

		addPopup(txtLog, popupMenu);
		mntmClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtLog.setText("");
			}
		});
		popupMenu.add(mntmClear);

		frmSyscRobot.setJMenuBar(menuBar);

		menuBar.add(mnFile);

		mnFile.add(closeWindow);

		menuBar.add(mnViews);

		mnViews.add(aboutMenu);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	private class ButtonListener implements ActionListener, ChangeListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			Object source = arg0.getSource();

			if (source instanceof JCheckBox) {
				JCheckBox tmp = (JCheckBox) source;
				if (tmp.equals(chckbxLed)) {

					communicator.writeData(tmp.isSelected() ? "001" : "000");

				} else if (tmp.equals(chckbxLed_1)) {
					communicator.writeData(tmp.isSelected() ? "011" : "010");

				} else if (tmp.equals(chckbxLed_2)) {
					communicator.writeData(tmp.isSelected() ? "021" : "020");

				} else if (tmp.equals(chckbxLed_3)) {
					communicator.writeData(tmp.isSelected() ? "031" : "030");

				} else if (tmp.equals(chckbxRelay)) {

					communicator.writeData(tmp.isSelected() ? "041" : "040");

				} else if (tmp.equals(chckbxBuzzer)) {

					communicator.writeData(tmp.isSelected() ? "051" : "050");

				}

			} else if (source instanceof JButton) {
				JButton tmp = (JButton) source;

				if (tmp.equals(btnUpdateLcd)) {

					String text = textField_1.getText();
					communicator.writeData("3");
					try {
						Thread.sleep(100);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					for (char b : text.toCharArray()) {						
						communicator.writeData("2"+b);
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
										
				} else

				if (tmp.equals(btnClearLcd)) {
					communicator.writeData("3");
				} else

				if (tmp.equals(btnCw)) {

					communicator.writeData("71");

				} else if (tmp.equals(btnCcw)) {
					communicator.writeData("70");
					
				} else if (tmp.equals(btnPullKeypad)) {
					communicator.writeData("4");
					btnPullKeypad.setEnabled(false);
					
				}

			} else if (source instanceof JComboBox) {
				@SuppressWarnings("unchecked")
				JComboBox<Integer> tmp = (JComboBox<Integer>) source;

				if (tmp.equals(combo7seg)) {
					communicator.writeData("1" + combo7seg.getSelectedItem());
				}

			}
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			// TODO Auto-generated method stub

			Object src = (Object) e.getSource();
			if (src instanceof JSlider) {
				JSlider slider = (JSlider) src;

				lblDutyValue.setText(slider.getValue() + "%");
				if (!slider.getValueIsAdjusting()) {
					// Get new value
					int duty = slider.getValue();
					if (duty == 100) {
						duty = 99;
					} // Bad..... oh :(
					communicator.writeData("6" + duty / 10 + duty % 10);
					
				}

			}

		}
	}
	
	class AboutDialog extends JDialog {

	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public AboutDialog() {

	        initUI();
	    }

	    public final void initUI() {

	        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

	        add(Box.createRigidArea(new Dimension(0, 10)));

	        //ImageIcon icon = new ImageIcon("notes.png");
	        //JLabel label = new JLabel(icon);
	        //label.setAlignmentX(0.5f);
	        //add(label);

	        add(Box.createRigidArea(new Dimension(0, 10)));

	        JLabel name = new JLabel("SYSC 2003 Robot Debugger");
	        name.setFont(new Font("Serif", Font.BOLD, 13));
	        name.setAlignmentX(0.5f);
	        add(name);
	        
	        String info ="        Amente Bekele & David Yao \n" +
	        		"        April 2013\n" +
	        		"       Special thanks to Professor Gabriel Wainer";
	        
	        JTextArea note = new JTextArea(info);
	        note.setAlignmentX(0.5f);
	        note.setEditable(false);
	        
	        add(note);
	        
	       
	        

	        add(Box.createRigidArea(new Dimension(0, 50)));

	        JButton close = new JButton("Close");
	        close.addActionListener(new ActionListener() {

	            public void actionPerformed(ActionEvent event) {
	                dispose();
	            }
	        });

	        close.setAlignmentX(0.5f);
	        add(close);

	        setModalityType(ModalityType.APPLICATION_MODAL);

	        setTitle("About");
	        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	        setLocationRelativeTo(null);
	        setSize(300, 200);
	    }
	}
	
}
